﻿using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    public class ProductController : Controller
    {
       
       public static List<ProductViewModel> productList = new List<ProductViewModel>()
            {
                new ProductViewModel(1,"Bola Basket", "Olahraga", 10000),
                new ProductViewModel(2,"Bola Futsal", "Olahraga", 10000),
                new ProductViewModel(3,"Buku Tulis", "ATK", 10000),
                new ProductViewModel(4,"Pemsil 3B", "ATK", 5000),
                new ProductViewModel(5,"Handsaplast", "Kesehatan", 3000),
                new ProductViewModel(6,"Tisu Basah", "Lainnya", 5000)
            };
        public IActionResult Index()
        {
            return View(productList);
        }
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Save([Bind("Id, Name, Category, Price")] ProductViewModel product)
        {
            productList.Add(product);
            return Redirect("Index");
        }
        public IActionResult Edit(int? id)
        {
            ProductViewModel product = productList.Find(x => x.Id.Equals(id));
            return View(product);
        }
        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Name, Category, Price")] ProductViewModel product)
        {
            ProductViewModel productOld = productList.Find(x => x.Id.Equals(id));
            productList.Remove(productOld);

            productList.Add(product);

            return View("Index");
        }

        public IActionResult Detail(int id)
        {
            ProductViewModel product = (from p in productList where p.Id.Equals(id) select p).SingleOrDefault(new ProductViewModel());

            return View(product);
        }
        public IActionResult Delete(int? id)
        {
            ProductViewModel product = productList.Find(x => x.Id.Equals(id));
            productList.Remove(product);

            return View("Index");
        }
        

        
    }
}
