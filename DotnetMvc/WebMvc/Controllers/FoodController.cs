﻿using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    public class FoodController : Controller
    {

        public static List<FoodViewModel> foodList = new List<FoodViewModel>()
        {
            new FoodViewModel(1,"Soto Bogor", "Makanan", 10000),
            new FoodViewModel(2,"Iga Bakar", "Makanan", 25000),
            new FoodViewModel(3,"Mie Goreng Spesial", "Makanan", 15000),
            new FoodViewModel(4,"Pecel", "Makanan", 10000),
            new FoodViewModel(6,"Ayam Goreng Madura", "Makanan", 15000),
            new FoodViewModel(7,"Es Dawt", "Minuman", 10000),
            new FoodViewModel(8,"Jus Nangla", "Minuman", 10000),
            new FoodViewModel(9,"Es Campur", "Minuman", 15000),
            new FoodViewModel(10,"Air Putih Dingin", "Minuman", 5000)

        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult List()
        {
            return View(foodList);
        }


        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Save([Bind("Id, Name, Category, Price")] FoodViewModel food)
        {
            foodList.Add(food);
            return Redirect("List");
        }
        public IActionResult Edit(int? id)
        {
            FoodViewModel food = foodList.Find(x => x.Id.Equals(id));
            return View(food);
        }
        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Name, Category, Price")] FoodViewModel food)
        {
            FoodViewModel foodOld = foodList.Find(x => x.Id.Equals(id));
            foodList.Remove(foodOld);
            foodList.Add(food);
            return View("List");
        }

        public IActionResult Detail(int id)
        {
            FoodViewModel food = (from c in foodList where c.Id.Equals(id) select c).SingleOrDefault(new FoodViewModel());

            return View(food);
        }
        public IActionResult Delete(int? id)
        {
            FoodViewModel food = foodList.Find(x => x.Id.Equals(id));
            foodList.Remove(food);

            return View("List");
        }



    }
}
