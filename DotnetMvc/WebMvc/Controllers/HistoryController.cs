﻿using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    public class HistoryController : Controller
    {
        public static List<HistoryViewModel> historyList = new List<HistoryViewModel>()
        {
            new HistoryViewModel(1, "Jacob", "HR", "On", "WFH"),
            new HistoryViewModel(2, "Jillian", "Artist", "On", "WFO"),
            new HistoryViewModel(3, "Friday", "Admin", "On", "WFO"),
            new HistoryViewModel(4, "Sunandar", "Staff", "On", "WFO"),
            new HistoryViewModel(5, "Miorinne", "Quality Control", "On", "WFO"),

        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult History()
        {
            return View(historyList);

        }

        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Save([Bind("Id, Name, Type, Brand, Price")] HistoryViewModel history)
        {
            historyList.Add(history);
            return Redirect("History");
        }
        public IActionResult Edit(int? id)
        {
            HistoryViewModel history = historyList.Find(x => x.Id.Equals(id));
            return View(history);
        }
        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Name, Job, Status, Details")] HistoryViewModel history)
        {
            HistoryViewModel HistoryOld = historyList.Find(x => x.Id.Equals(id));
            historyList.Remove(HistoryOld);
            historyList.Add(history);

            return View("History");
        }

        public IActionResult Detail(int id)
        {
            HistoryViewModel history = (from h in historyList where h.Id.Equals(id) select h).SingleOrDefault(new HistoryViewModel());

            return View(history);
        }
        public IActionResult Delete(int? id)
        {
            HistoryViewModel history = historyList.Find(x => x.Id.Equals(id));
            historyList.Remove(history);

            return View("History");
        }
    }
}
