﻿using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    public class CarController : Controller
    {

        public static List<CarViewModel> carList = new List<CarViewModel>()
        {
            new CarViewModel(1, "Avanza", "Family Car", "Toyota", 65000000),
            new CarViewModel(2, "Xenia", "Family Car", "Daihatsu", 50000000),
            new CarViewModel(3, "GTR R35", "Sport Car", "Nissan", 300000000),
            new CarViewModel(4, "Pathfinder", "Family Car", "Nissan", 100000000),
            new CarViewModel(5, "Supra", "Sport Car", "Toyota", 10000000),
            new CarViewModel(6, "NSX", "Sport Car", "Honda", 1050000)

        };
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult List()
        {
            return View(carList);
        }


        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Save([Bind("Id, Name, Type, Brand, Price")] CarViewModel car)
        {
            carList.Add(car);
            return Redirect("List");
        }
        public IActionResult Edit(int? id)
        {
            CarViewModel car = carList.Find(x => x.Id.Equals(id));
            return View(car);
        }
        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Name, Type, Brand, Price")] CarViewModel car)
        {
            CarViewModel carLame = carList.Find(x => x.Id.Equals(id));
            carList.Remove(carLame);
            carList.Add(car);

            return View("List");
        }

        public IActionResult Detail(int id)
        {
            CarViewModel car = (from c in carList where c.Id.Equals(id) select c).SingleOrDefault(new CarViewModel());

            return View(car);
        }
        public IActionResult Delete(int? id)
        {
            CarViewModel car = carList.Find(x => x.Id.Equals(id));
            carList.Remove(car);

            return View("List");
        }



    }

}
