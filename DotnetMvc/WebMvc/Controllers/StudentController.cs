﻿using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    public class StudentController : Controller
    {
        public static List<StudentViewModel> studentList = new List<StudentViewModel>()
        {
            new StudentViewModel(1,"Shidqi", "Cibarusah", "Tidur", "Tidur adalah koentji"),
            new StudentViewModel(2,"Mario", "Cibaduyut", "Membetulkan Pipa", "Mamamia"),
            new StudentViewModel(3,"Luigi", "Cibaduyut", "Membantu Mario", "Marrioooo"),
            new StudentViewModel(4,"Link", "Sumberwarna", "Menyelamatkan Zelda", "Hiyaahiyaa")

        };

        public IActionResult List()
        {
            return View(studentList);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Save([Bind("Id, Name, Type, Brand")] StudentViewModel student)
        {
            studentList.Add(student);
            return Redirect("List");
        }
        public IActionResult Edit(int? id)
        {
            StudentViewModel student = studentList.Find(x => x.Id.Equals(id));
            return View(student);
        }
        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Name, Type, Brand")] StudentViewModel student)
        {
            StudentViewModel studentOld = studentList.Find(x => x.Id.Equals(id));
            studentList.Remove(studentOld);
            studentList.Add(student);

            return View("List");
        }

        public IActionResult Detail(int id)
        {
            StudentViewModel student = (from c in studentList where c.Id.Equals(id) select c).SingleOrDefault(new StudentViewModel());

            return View(student);
        }
        public IActionResult Delete(int? id)
        {
            StudentViewModel student = studentList.Find(x => x.Id.Equals(id));
            studentList.Remove(student);

            return View("List");
        }
    }
}
