﻿using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    public class AbsentController : Controller
    {
        public static List<AbsentViewModel> absentList = new List<AbsentViewModel>()
        {
            new AbsentViewModel(1, 123, "Shidqi"),
            new AbsentViewModel(2, 223, "Rama"),
            new AbsentViewModel(3, 123, "Rizqi")
        };
        public IActionResult Index()
        {

            return View();
        }

        public IActionResult List()
        {
            return View(absentList);
        }

        [HttpPost]
        public IActionResult Save([Bind("EmployeeId, Name, AbsentStartDate, AbsentEndDate, Location, Description")] AbsentViewModel absent)
        {
            absentList.Add(absent);
            return Redirect("Index");
        }

        public IActionResult Out(int? id)
        {
            AbsentViewModel absent = absentList.Find(x => x.Id.Equals(id));
            return View(absent);
           
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind ("EmployeeId, Name, AbsentStartDate, AbsentEndDate, Location, Description")] AbsentViewModel absent)
        {
            AbsentViewModel absentOld = absentList.Find(x => x.Id.Equals(id));
            absentList.Remove(absentOld);
            absentList.Add(absent);

            return View("List");
        }


    }
}
