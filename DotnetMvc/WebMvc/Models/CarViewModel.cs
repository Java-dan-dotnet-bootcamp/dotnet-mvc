﻿namespace WebMvc.Models
{
    public class CarViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Brand { get; set; }
        public long Price { get; set; }

        public CarViewModel() { }

        public CarViewModel(int id, string name, string type, string brand, long price)
        {
            Id = id;
            Name = name;
            Type = type;
            Brand = brand;
            Price = price;
        }
    }
}
