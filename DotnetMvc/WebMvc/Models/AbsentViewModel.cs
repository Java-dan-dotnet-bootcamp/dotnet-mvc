﻿using System.Xml.Linq;

namespace WebMvc.Models
{
    public class AbsentViewModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public DateTime AbsentStartDate { get; set; }
        public DateTime AbsentEndDate { get; set; }
        public String Location { get; set; }
        public String Description { get; set; }

        public AbsentViewModel() { }

       public AbsentViewModel(int id, int employeeId, string name, DateTime absentStartDate, DateTime absentEndDate, string location, string description) : this(id, employeeId, name)
        {
            Id = id;
            EmployeeId = employeeId;
            Name = name;
            AbsentStartDate = absentStartDate;
            AbsentEndDate = absentEndDate;
            Location = location;
            Description = description;
        }

        public AbsentViewModel(int id, int employeeId, string name)
        {
            Id = id;
            EmployeeId = employeeId;
            Name = name;

        }
    }
}
