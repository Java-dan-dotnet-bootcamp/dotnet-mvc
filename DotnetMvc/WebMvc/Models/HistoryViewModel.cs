﻿namespace WebMvc.Models
{
    public class HistoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Job { get; set; }
        public string Status { get; set; }
        public string Details { get; set; }

        public HistoryViewModel() { }

        public HistoryViewModel(int id, string name, string job, string status, string details)
        {
            Id = id;
            Name = name;
            Job = job;
            Status = status;
            Details = details;
        }
    }
}
