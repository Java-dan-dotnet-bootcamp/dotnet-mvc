﻿namespace WebMvc.Models
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Hobby { get; set; }

        public string Motto { get; set; }
        

        public StudentViewModel() { }

        public StudentViewModel(int id, string name, string address, string hobby, string motto)
        {
            Id = id;
            Name = name;
            Address = address;
            Hobby = hobby;
            Motto = motto;
        }
    }
}
